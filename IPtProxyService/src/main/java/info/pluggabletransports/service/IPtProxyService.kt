package info.pluggabletransports.service

import IPtProxy.IPtProxy
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.util.*

class IPtProxyService : Service() {


    val ACTION_START = "info.pluggabletransports.start"
    val ACTION_STOP = "info.pluggabletransports.stop"
    val ACTION_STATUS = "info.pluggabletransports.status"

    val EXTRA_TYPE = "info.pluggabletransports.type"
    val EXTRA_CONFIG = "info.pluggabletransports.config"

    val EXTRA_PACKAGE_NAME = "xtrpkg"
    val EXTRA_PORT = "info.pluggabletransports.port"

    private var NOTIFY_ID = 97;

    var TYPE_SNOWFLAKE = "snowflake"
    var TYPE_OBFS4 = "obfs4"

    override fun onCreate() {
        super.onCreate()
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {

        startForeground(NOTIFY_ID, App.getNotification(this));

        var intentCaller = intent.getStringExtra(EXTRA_PACKAGE_NAME)

        if (intent.action.equals(ACTION_START)) {

            loadCdnFronts(applicationContext)

            //if no type specified, or type is snowflake
            if ((!intent.hasCategory(EXTRA_TYPE))||intent.getStringExtra(EXTRA_TYPE).equals(TYPE_SNOWFLAKE)) {

                startSnowflakeClient()

                val intentStatus = Intent(ACTION_STATUS);
                intentStatus.setPackage(intentCaller)
                intentStatus.putExtra(EXTRA_TYPE, TYPE_SNOWFLAKE)
                intentStatus.putExtra(EXTRA_PORT, IPtProxy.snowflakePort())
                sendBroadcast(intentStatus)
            }
            else if (intent.getStringExtra(EXTRA_TYPE).equals(TYPE_OBFS4)) {
                val proxy = intent.getStringExtra(EXTRA_CONFIG);
                IPtProxy.startObfs4Proxy("INFO", false, false, proxy);

                val intentStatus = Intent(ACTION_STATUS);
                intentStatus.setPackage(intentCaller)
                intentStatus.putExtra(EXTRA_TYPE, TYPE_OBFS4)
                intentStatus.putExtra(EXTRA_PORT, IPtProxy.obfs4Port())
                sendBroadcast(intentStatus)
            }
        }
        else if (intent.action.equals(ACTION_STOP))
        {
            IPtProxy.stopSnowflake();
            IPtProxy.stopObfs4Proxy();

            stopForeground(true)
        }
        else if (intent.action.equals(ACTION_STATUS))
        {
            val intentStatus = Intent(ACTION_STATUS);
            intentStatus.setPackage(intentCaller)
            intentStatus.putExtra(EXTRA_TYPE, TYPE_OBFS4)
            intentStatus.putExtra(EXTRA_PORT, IPtProxy.obfs4Port())
            sendBroadcast(intentStatus)

            val intentStatusSnowflake = Intent(ACTION_STATUS);
            intentStatusSnowflake.setPackage(intentCaller)
            intentStatusSnowflake.putExtra(EXTRA_TYPE, TYPE_SNOWFLAKE)
            intentStatusSnowflake.putExtra(EXTRA_PORT, IPtProxy.snowflakePort())
            sendBroadcast(intentStatusSnowflake)
        }


        return super.onStartCommand(intent, flags, startId)
    }

    private fun startSnowflakeClient() {
        //this is using the current, default Tor snowflake infrastructure
        val target = getCdnFront("snowflake-target")
        val front = getCdnFront("snowflake-front")
        val ice = getCdnFront("snowflake-stun")

        IPtProxy.startSnowflake(ice, target, front, null, null, false, true, false, 1);


    }

    override fun onDestroy() {
        super.onDestroy()
        IPtProxy.stopSnowflake()
    }


    inner class LocalBinder : Binder() {
        fun getService(): IPtProxyService = this@IPtProxyService

    }

    private val binder: IBinder = LocalBinder()

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    companion object {

        lateinit var ACTION_START: String
        lateinit var ACTION_STOP: String
        lateinit var EXTRA_TYPE: String
        lateinit var EXTRA_PACKAGE_NAME: String


        private var mFronts: HashMap<String, String>? = null
        fun getCdnFront(service: String): String? {
            return mFronts!![service]
        }

        fun loadCdnFronts(context: Context) {
            if (mFronts == null) {
                mFronts = HashMap<String,String>()
                try {
                    val reader = BufferedReader(InputStreamReader(context.assets.open("fronts")))
                    var line: String
                    reader.forEachLine {

                        var parts = it.split(' ')

                        val key = parts.get(0) as String
                        var value = parts.get(1) as String

                        if (value != null)
                            mFronts!![key] = value


                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }
}